function App() {

  const edad = 20

  return (
    <>
      { edad >= 18 ? 'Mayor de edad' : 'Menor de edad'}
      <div>
        <h1>Pruebas React {'Daniel Reyes'.toUpperCase()}</h1>
        <p>Un parrafo</p>
      </div>
    </>

  )
}

export default App
