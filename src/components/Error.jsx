// Forma 1 de llamar a un prop - individual
/*const Error = ({mensaje}) => {
  return (
    <div className='bg-red-700 text-white text-center uppercase p-3 font-bold mb-3 rounded-md'>
        <p> {mensaje} </p>
    </div>
  )
}*/

// Forma 2 de llamar a un prop - todo
const Error = ({children}) => {
    return (
      <div className='bg-red-700 text-white text-center uppercase p-3 font-bold mb-3 rounded-md'>
          {children}
      </div>
    )
  }

export default Error