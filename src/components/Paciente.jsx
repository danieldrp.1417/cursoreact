const Paciente = ({paciente, setPaciente, eliminarPaciente}) => {

    const {nombre, propietario, mail, alta, sintomas, id} = paciente

    const handleEliminar = () => {
        const respuesta = confirm('Deseas eliminar el paciente?')
        if (respuesta) {
            eliminarPaciente(id)
        }
    }

    return (
        <div className="m-3 bg-white shadow-md px-5 py-10 rounded-xl">
            <p className="font-bold mb-3 text-gray-600 uppercase">
                Nombre: {''}
                <span className="font-normal normal-case">{nombre}</span>
            </p>
            <p className="font-bold mb-3 text-gray-600 uppercase">
                Propietario: {''}
                <span className="font-normal normal-case">{propietario}</span>
            </p>
            <p className="font-bold mb-3 text-gray-600 uppercase">
                Email: {''}
                <span className="font-normal normal-case">{mail}</span>
            </p>
            <p className="font-bold mb-3 text-gray-600 uppercase">
                Fecha Alta: {''}
                <span className="font-normal normal-case">{alta}</span>
            </p>
            <p className="font-bold mb-3 text-gray-600 uppercase">
                Sintomas: {''}
                <span className="font-normal normal-case">{sintomas}</span>
            </p>
            
            <div className="flex justify-between mt-10">
                <button type="button" 
                    className="py-2 px-10 bg-green-700 hover:bg-green-800 text-white text-center font-bold uppercase rounded-md"
                    onClick={() => setPaciente(paciente)}>
                    Editar
                </button>
                <button type="button"
                    className="py-2 px-10 bg-red-700 hover:bg-red-800 text-white text-center font-bold uppercase rounded-md"
                    onClick={handleEliminar}>
                    Eliminar
                </button>
            </div>
        </div>
    )
}

export default Paciente