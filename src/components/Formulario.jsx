import {useState, useEffect} from 'react';
import Error from './Error';

const Formulario = ({pacientes, setPacientes, paciente, setPaciente}) => {

    // Definimos el estado del hook
    const [nombre, setNombre] = useState('')
    const [propietario, setPropietario] = useState('')
    const [mail, setMail] = useState('')
    const [alta, setAlta] = useState('')
    const [sintomas, setSintomas] = useState('')

    const [error, setError] = useState(false)

    useEffect(() => {
        if (Object.keys(paciente).length > 0) {
            setNombre(paciente.nombre)
            setPropietario(paciente.propietario)
            setMail(paciente.mail)
            setAlta(paciente.alta)
            setSintomas(paciente.sintomas)
        }
    }, [paciente])

    const generarId = () => {
        const random = Math.random().toString(36).substr(2)
        const fecha = Date.now().toString(36)

        return random + fecha
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        // Validacion de formulario
        if ([nombre, propietario, mail, alta, sintomas].includes('')) {
            setError(true)
            return
        }
        
        setError(false)

        // Objeto de paciente
        const objetoPaciente = {
            nombre,
            propietario,
            mail,
            alta,
            sintomas
        }

        if (paciente.id) {
            // Editando registro
            objetoPaciente.id = paciente.id

            const pacientesActualizados = pacientes.map(pacienteState => pacienteState.id === paciente.id
                ? objetoPaciente : pacienteState)

            setPacientes(pacientesActualizados)
            setPaciente({})
        } else {
            // Nuevo registro
            objetoPaciente.id = generarId()
            setPacientes([...pacientes, objetoPaciente])
        }

        //Reiniciamos el formulario
        setNombre('')
        setPropietario('')
        setMail('')
        setAlta('')
        setSintomas('')

    }

    return (
        <div className="md:w-1/2 lg:w-2/5 mx-5">
            <h2 className="font-black text-3xl text-center">Seguimiento Paciente</h2>

            <p className="text-lg mt-5 text-center mb-10">
                Añade Pacientes y {""}
                <span className="text-indigo-700 font-bold">Administralos</span>
            </p>

            <form className="bg-white shadow-md rounded-lg py-10 px-5 mb-10" onSubmit={handleSubmit}>
                {/*  error && <Error mensaje='Todos los campos son obligatorios' /> -- Forma 1 de pasar un prop }
                    Abajo la forma dos, se pone entre los keys del componente */
                    error && <Error> <p>Todos los campos son obligatorios</p> </Error>
                }
                <div className="mb-5">
                    <label className="block text-gray-700 uppercase font-bold" htmlFor="nombre">Nombre</label>
                    <input id="nombre" type="text" placeholder="Nombre de la mascota"
                        className="border-2 w-full p-2 mt-2 placeholder-sky-400 rounded-md"
                        value={nombre}
                        onChange={ (e) => setNombre(e.target.value)} />
                </div>
                <div className="mb-5">
                    <label className="block text-gray-700 uppercase font-bold" htmlFor="propietario">Propietario</label>
                    <input id="propietario" type="text" placeholder="Nombre del dueño"
                        className="border-2 w-full p-2 mt-2 placeholder-sky-400 rounded-md"
                        value={propietario}
                        onChange={ (e) => setPropietario(e.target.value)} />
                </div>
                <div className="mb-5">
                    <label className="block text-gray-700 uppercase font-bold" htmlFor="mail">E-mail</label>
                    <input id="mail" type="email" placeholder="Correo para notificaciones"
                        className="border-2 w-full p-2 mt-2 placeholder-sky-400 rounded-md"
                        value={mail}
                        onChange={ (e) => setMail(e.target.value)} />
                </div>
                <div className="mb-5">
                    <label className="block text-gray-700 uppercase font-bold" htmlFor="alta">Alta</label>
                    <input id="alta" type="date"
                        className="border-2 w-full p-2 mt-2 rounded-md"
                        value={alta}
                        onChange={ (e) => setAlta(e.target.value)} />
                </div>
                <div className="mb-5">
                    <label className="block text-gray-700 uppercase font-bold" htmlFor="sintomas">Sintomas</label>
                    <textarea id="sintomas" placeholder="Describe los sintomas del paciente"
                        className="border-2 w-full p-2 mt-2  placeholder-sky-400 rounded-md"
                        value={sintomas}
                        onChange={ (e) => setSintomas(e.target.value)} />
                </div>

                <input type="submit" value={ paciente.id ? 'Editar paciente' : 'Agregar paciente'}
                    className="bg-sky-400 w-full p-3 text-white uppercase font-bold hover:bg-sky-800
                        cursor-pointer transition-all" />
            </form>

        </div>
    )

}

export default Formulario